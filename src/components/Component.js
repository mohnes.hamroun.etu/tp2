export default class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	renderAttribute() {
		if (!this.attribute) {
			return '';
		}
		return `${this.attribute.name} ="${this.attribute.value}"`;
	}

	render() {
		if (!this.children) {
			return `<${this.tagName} ${this.renderAttribute()}/>`;
		}
		return `<${this.tagName} ${this.renderAttribute()}>${this.children}</${
			this.tagName
		}>`;
	}
}
